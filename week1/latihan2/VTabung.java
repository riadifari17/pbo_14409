public class VTabung {

    double dia, tinggi;

    double Vol_tabung() {
        dia = 5;
        tinggi = 10;

        return 3.14 * (dia / 2) * (dia / 2) * tinggi;
    }

    void output() {
        System.out.println(Vol_tabung());
    }

    public static void main(String[] args) {

        VTabung v_tabung = new VTabung();
        v_tabung.output();
    }
}
